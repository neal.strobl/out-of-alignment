//user actions
export const LOGIN = 'LOGIN';
export const LOGIN_CHANGED = 'LOGIN_CHANGED';
export const LOGOUT = 'LOGOUT';
export const UPDATE_PASSWORD = 'UPDATE_PASSWORD';
export const SEND_PASSWORD_RESET = 'SEND_PASSWORD_RESET';

//effect actions
export const CAST_FIREBALL = 'CAST_FIREBALL';
export const QUENCH_FIREBALL = 'QUENCH_FIREBALL';

//from user
export const UPDATE_PLAYER = 'UPDATE_PLAYER';

//from db
export const PLAYER_UPDATED = 'PLAYER_UPDATED';
export const FIREBALL_UPDATED = 'FIREBALL_UPDATED';
export const ERROR = 'ERROR';
