import UIfx from 'uifx';
import * as explosions from "react-explode";

import partyMp3 from '../../sounds/party.mp3';
import screamMp3 from '../../sounds/scream.mp3';
import healMp3 from '../../sounds/heal.mp3';

export const CONFETTI_CONFIG = {
  angle: "90",
  spread: 90,
  startVelocity: "20",
  elementCount: "30",
  dragFriction: 0.1,
  duration: 3000,
  stagger: 0,
  width: "4px",
  height: "4px",
  colors: ["#a864fd", "#29cdff", "#78ff44", "#ff718d", "#fdff6a"]
};

export const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});

export const PartySound = new UIfx(partyMp3);
export const ScreamSound = new UIfx(screamMp3);
export const HealSound = new UIfx(healMp3);

export const Fireball = explosions["Negros"];
export const CureWounds = explosions["Palawan"];