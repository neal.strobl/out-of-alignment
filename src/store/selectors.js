export const getErrors = state => state.errors;
export const isPendingLogin = state => state.user.loginPending;
export const isPendingLogout = state => state.user.logoutPending;
export const isPendingUpdatePassword = state => state.user.updatePasswordPending;
export const isPendingPasswordReset = state => state.user.passwordResetPending;
export const getNumberOfLoginAttempts = state => state.user.loginAttempts;
export const isFailedLogin = state => { return state.user.loginAttempts > 0 };
export const isLoggedIn = state => state.user.loggedIn;

export const getPlayers = state => state.db.players;
export const getCaroline = state => state.db.players.find(player => player.player === 'Caroline');
export const getNeal = state => state.db.players.find(player => player.player === 'Neal');
export const getGreg = state => state.db.players.find(player => player.player === 'Greg');
export const getMark = state => state.db.players.find(player => player.player === 'Mark');
export const getPendingPlayerActions = state => state.db.pendingActions;

export const getFireball = state => state.effects? state.effects.fireball: false;