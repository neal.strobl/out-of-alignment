import * as types from './constants/actionTypes';
import * as firebaseAuth from '../firebase/firebaseAuth'; 
import { doUpdatePlayerDBEntry } from '../firebase/firebasePlayers'; 
import { doUpdateFireball } from '../firebase/firebaseEffects'; 

export const login = (email, password) => ({
      type: types.LOGIN,
      payload: new Promise((resolve,reject) => {
        firebaseAuth.doSignInWithEmailAndPassword(email,password).then(() => {
          resolve();
        }).catch(err => {
          reject(err);
        })
      })
})

export const loginChanged = (loggedIn) => ({
    type: types.LOGIN_CHANGED,
    payload: loggedIn
})

export const logout = () => ({
  type: types.LOGOUT,
  payload: new Promise((resolve,reject) => {
    firebaseAuth.doSignOut().then(() => {
      resolve();
    }).catch(err => {
      reject(err);
    })
  })
})

export const resetPassword = (email) => ({
  type: types.SEND_PASSWORD_RESET,
  payload: new Promise((resolve,reject) => {
    firebaseAuth.doPasswordReset(email).then(() => {
      resolve();
    }).catch(err => {
      reject(err);
    })
  })
})

//used by realtime db updates
export const playerUpdated = (character, d4, d6, inspiration, player, heal) => ({
  type: types.PLAYER_UPDATED,
  payload: { 
    character: character,
    d4: d4,
    d6: d6,
    inspiration: inspiration,
    player: player,
    heal: heal,
  }
});

export const updatePlayer = (player, buttonId) => ({
  type: types.UPDATE_PLAYER,
  payload: new Promise((resolve,reject) => {
    doUpdatePlayerDBEntry(player).then(() => {
      resolve();
    }).catch(err => {
      reject(err);
    })
  }),
  meta: buttonId
});

export const error = (err) => ({
  type: types.ERROR,
  payload: err
});

export const fireballUpdated = (triggered) => ({
  type: types.FIREBALL_UPDATED,
  payload: triggered
});

export const castFireball = () => ({
  type: types.CAST_FIREBALL,
  payload: new Promise((resolve,reject) => {
    doUpdateFireball(true).then(() => {
      resolve();
    }).catch(err => {
      reject(err);
    })
  })
});

export const quenchFireball = () => ({
  type: types.QUENCH_FIREBALL,
  payload: new Promise((resolve,reject) => {
    doUpdateFireball(false).then(() => {
      resolve();
    }).catch(err => {
      reject(err);
    })
  })
});