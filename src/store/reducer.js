import { ActionType } from 'redux-promise-middleware';
import * as actions from './constants/actionTypes';

const initialState = {
  user: {
    loginPending: false,
    logoutPending: false,
    passwordResetPending: false,
    loginAttempts: 0,
    loggedIn: false
  },
  db: {
    players: [
      {
        player: 'Caroline',
        character: '',
        d4: true,
        d6: true,
        inspiration: true,
        heal: false,
      },
      {
        player: 'Neal',
        character: '',
        d4: true,
        d6: true,
        inspiration: true,
        heal: false,
      },
      {
        player: 'Greg',
        character: '',
        d4: true,
        d6: true,
        inspiration: true,
        heal: false,
      },
      {
        player: 'Mark',
        character: '',
        d4: true,
        d6: true,
        inspiration: true,
        heal: false,
      }
    ], //contains db synced player data
    pendingActions: [], //contains ids for buttons with currently pending actions
  },
  effects: {
    fireball: false,
  },
  errors: [],
};

const reducer = (state = initialState, action) => {
  let newState = {
    user: userReducer(state.user, action),
    db: dbReducer(state.db, action),
    effects: effectsReducer(state.effects, action),
    errors: errorsReducer(state.errors, action)
  }
  return newState;
};

const userReducer = (state, action) => {
  switch(action.type){
    case actions.LOGIN_CHANGED:
      return {
        ...state,
        loggedIn: action.payload
      }
    // Pending ACTIONS //
    case actions.LOGIN.concat("_").concat(ActionType.Pending):
      return {
        ...state,
        loginPending: true
      };
    case actions.LOGOUT.concat("_").concat(ActionType.Pending):
      return {
        ...state,
        logoutPending: true
      };
    case actions.SEND_PASSWORD_RESET.concat("_").concat(ActionType.Pending):
      return {
        ...state,
        passwordResetPending: true
      };
    //Fulfilled ACTIONS //
    case actions.LOGIN.concat("_").concat(ActionType.Fulfilled):
      return {
        ...state,
        loginAttempts: 0,
        loginPending: false
      };
    case actions.LOGOUT.concat("_").concat(ActionType.Fulfilled):
      return {
        ...state,
        logoutPending: false
      };
    case actions.SEND_PASSWORD_RESET.concat(ActionType.Fulfilled):
      return {
        ...state,
        passwordResetPending: false
      };
    case actions.LOGIN.concat("_").concat(ActionType.Rejected):
      return {
        ...state,
        loginAttempts: state.loginAttempts + 1,
        loginPending: false
      };
    case actions.LOGOUT.concat("_").concat(ActionType.Rejected):
      return {
        ...state,
        logoutPending: false
      };
    case actions.SEND_PASSWORD_RESET.concat("_").concat(ActionType.Rejected):
      return {
        ...state,
        passwordResetPending: false
      };
    default:
      return state;
  }
};

const dbReducer = (state, action) => {
  switch(action.type){
    case actions.UPDATE_PLAYER.concat("_").concat(ActionType.Pending):
      return {
        ...state,
        pendingActions: [
          ...state.pendingActions,
          action.meta
        ]
      }
    case actions.UPDATE_PLAYER.concat("_").concat(ActionType.Fulfilled):
    case actions.UPDATE_PLAYER.concat("_").concat(ActionType.Rejected):
      let nonRemovedPendingActions = state.pendingActions.filter(pendingAction => pendingAction !== action.meta)
      return {
        ...state,
        pendingActions: nonRemovedPendingActions
      }
    case actions.PLAYER_UPDATED:
      let staticPlayers = state.players.filter(player => player.player !== action.payload.player);
      return {
        ...state,
        players: [
          ...staticPlayers,
          { 
            character: action.payload.character, 
            d4: action.payload.d4, 
            d6: action.payload.d6, 
            inspiration: action.payload.inspiration, 
            player: action.payload.player,
            heal: action.payload.heal,
          }
        ]
      }
    default:
      return state;
  }
};

const effectsReducer = (state, action) => {
  switch(action.type){
    case actions.FIREBALL_UPDATED:
      return {
        ...state, fireball: action.payload
      }
    default: {
      return state;
    }
  }
}

const errorsReducer = (state, action) => {
  switch(action.type){
    case actions.ERROR:
    case actions.LOGIN.concat("_").concat(ActionType.Rejected):
    case actions.LOGOUT.concat("_").concat(ActionType.Rejected):
    case actions.UPDATE_PASSWORD.concat("_").concat(ActionType.Rejected):
    case actions.SEND_PASSWORD_RESET.concat("_").concat(ActionType.Rejected):
      return [
        ...state,
        action.payload
      ]
    default:
      return state;
  }
}

export default reducer;