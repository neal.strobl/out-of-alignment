import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles }from '@material-ui/core';
import * as explosions from "react-explode";

import Icons from '../components/Icons';

import { ScreamSound } from '../store/constants/util';

import background from '../images/background.png';
import backgroundParty from '../images/background_party.png';

const Fireball = explosions["Negros"];

class Game extends Component {
  render() {
    const { 
      classes,
      fireball,
      player1,
      player2,
      player3,
      player4,
    } = this.props;
    return (
      <div className={classes.App}>
        {
          fireball &&
          <div className={classes.fireball}>
            <Fireball color="red" size="600" delay={0} repeat={0} onStart={ScreamSound.play(0.3)} />
          </div>
        }
        <div className={classes.icons}>
          <Icons className={classes.icons1} d4={player1.d4} d6={player1.d6} inspiration={player1.inspiration} heal={player1.heal}/>
          <Icons className={classes.icons2} d4={player2.d4} d6={player2.d6} inspiration={player2.inspiration} heal={player2.heal}/>
          <Icons className={classes.icons3} d4={player3.d4} d6={player3.d6} inspiration={player3.inspiration} heal={player3.heal}/>
          <Icons className={classes.icons4} d4={player4.d4} d6={player4.d6} inspiration={player4.inspiration} heal={player4.heal}/>
        </div>
      </div>
    )
  }
}

const game_main_styles = (theme) => ({
  App: {
    backgroundColor: "transparent",
    //backgroundImage: `url(${background})`,
    minHeight: "100vh",
    minWidth: "100vw",
  },
  icons: {
    position: "fixed",
    top: "817px",
    left: "151px",
    backgroundColor: "transparent",
    width: "480px",
    height: "256px",
  },
  icons1: {
    position: "absolute",
    top: "0",
    left: "0",
    width: "50px",
    height: "188px",
  },
  icons2: {
    position: "absolute",
    top: "0px",
    left: "436px",
    width: "50px",
    height: "188px",
  },
  icons3: {
    position: "absolute",
    top: "132px",
    left: "0",
    width: "50px",
    height: "188px",
  },
  icons4: {
    position: "absolute",
    top: "132px",
    left: "436px",
    width: "50px",
    height: "188px",
  },
  
});

const party_main_styles = (theme) => ({
  App: {
    backgroundColor: "transparent",
    //backgroundImage: `url(${backgroundParty})`,
    minHeight: "100vh",
    minWidth: "100vw",
  },
  icons: {
    position: "fixed",
    top: "385px",
    left: "762px",
    backgroundColor: "transparent",
    width: "685px",
    height: "660px",
  },
  icons1: {
    position: "absolute",
    top: "0",
    left: "0",
    width: "50px",
    height: "188px",
  },
  icons2: {
    position: "absolute",
    top: "0px",
    left: "640px",
    width: "50px",
    height: "188px",
  },
  icons3: {
    position: "absolute",
    top: "540px",
    left: "0",
    width: "50px",
    height: "188px",
  },
  icons4: {
    position: "absolute",
    top: "540px",
    left: "640px",
    width: "50px",
    height: "188px",
  },
});


Game.propTypes = {
  player1: PropTypes.object.isRequired,
  player2: PropTypes.object.isRequired,
  player3: PropTypes.object.isRequired,
  player4: PropTypes.object.isRequired,
  fireball: PropTypes.bool.isRequired,
};

Game.defaultProps = {
  fireball: false,
}
const GameMain = withStyles(game_main_styles)(Game);
const PartyMain = withStyles(party_main_styles)(Game);

export {
  GameMain,
  PartyMain,
}