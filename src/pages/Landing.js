import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {
  withStyles,
  Grid,
  Button,
  IconButton,
  Typography,
  FormControl,
  Input,
  InputAdornment,
  InputLabel,
  Snackbar }from '@material-ui/core';

import {
  Visibility,
  VisibilityOff,
  Close }from '@material-ui/icons';

  import d4_off from '../images/icons/d4-off.png';
import d4_on from '../images/icons/d4-on.png';
import d6_off from '../images/icons/d6-off.png';
import d6_on from '../images/icons/d6-on.png';
import inspiration_off from '../images/icons/inspiration-off.png';
import inspiration_on from '../images/icons/inspiration-on.png';
import healingIcon from '../images/icons/healing.png';
import fireballIcon from '../images/icons/dice-fire.png'

import { Fireball, CureWounds } from '../store/constants/util'

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
})

export class Landing extends Component {
  constructor(props){
    super(props);
    this.state = { 
      email: '',
      password: '',
      showPassword: false,
      snackbarOpen: false,
      snackbarMessage: '',
    }
  }

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleClickShowPassword = () => {
    this.setState({ ...this.state, showPassword: !this.state.showPassword });
  };

  handlePasswordReset = () => {
    if(this.state.email !== ''){
      this.props.resetPassword(this.state.email);
      this.setState({
        ...this.state,
        snackbarOpen: true,
        snackbarMessage: "Password reset email sent"
      })
    }else{
      this.setState({
        ...this.state,
        snackbarOpen: true,
        snackbarMessage: "Email must be specified to send forgot password email"
      })
    }
  }

  handleLogin = () => {
    const {
      email,
      password,
    } = this.state;

    this.props.login(email, password);
  }

  handleLogout = () => {
    this.props.logout();
  }

  handleSnackbarClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({
      ...this.state,
      snackbarOpen: false
    })
  };

  /**
   * Renders a button with icon for use with logged-in view
   * @param {*} icon Icon to display
   * @param {string} text Text to display
   * @param {*} onClickFunc onClick function for button
   */
  renderButton(icon, text, onClickFunc = () => {}){
    const{
      classes
    } = this.props;
    return(
      <Grid item xs={6}><Button variant="contained" className={classes.playerButton} startIcon={<img src={icon} alt={text}/>} onClick={onClickFunc}>{text}</Button></Grid>
    );
  }

  render() {
    const { 
      classes,
      player1,
      player2,
      player3,
      player4,
      isFailedLogin,
      isPendingLogin,
      isPendingLogout,
      updatePlayer,
      isLoggedIn,
      quenchFireball,
      castFireball,
      fireball,
    } = this.props;

    const {
      email,
      password,
      snackbarOpen,
      snackbarMessage
    } = this.state;

    const isInvalid = 
      password === '' ||
      email === '';

    return (
      <div className={isLoggedIn? classes.App_dark: classes.App}>
      {
        isLoggedIn && fireball &&
        <div className={classes.fireballLoggedIn}>
          <Fireball color="red" size="600" delay={1} repeat={0} onComplete={() => quenchFireball()}/>
        </div>
      }
      {
        !isLoggedIn &&
        <form onSubmit={this.onSubmit} className={classes.formContainer}>
          <FormControl required={true} fullWidth={true} margin='normal'>
            <InputLabel htmlFor="email">Email</InputLabel>
            <Input
              id="email"
              value={email}
              onChange={event => this.setState(byPropKey('email', event.target.value))}
              type="email"
            />
          </FormControl>
          <FormControl required={true} fullWidth={true} margin='normal'>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              id="password"
              onChange={event => this.setState(byPropKey('password', event.target.value))}
              type={this.state.showPassword ? 'text' : 'password'}
              value={password}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={this.handleClickShowPassword}
                    onMouseDown={this.handleMouseDownPassword}
                  >
                    {this.state.showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
          
          <Grid container className={classes.buttonContainer} direction='row' justify='space-between'>
            <Button variant="outlined" color="secondary" type="button" disabled={!isFailedLogin} onClick={() => this.handlePasswordReset()}>
              Forgot Password?
            </Button>

            <Button variant="contained" color="primary" type="button" disabled={isInvalid || isPendingLogin} onClick={() => this.handleLogin()} >
              Login
            </Button>
          </Grid>
        </form>
      }
      {
        isLoggedIn && !isPendingLogin &&
        <Grid container spacing={1} className={classes.loggedInRoot}>
          {
            player1.heal &&
            <div className={classes.cureWoundsPlayer1}>
              <CureWounds color="lime" size="300" delay={1} repeat={0} onComplete={() => updatePlayer({...player1, heal: false})}/>
            </div>
          }
          {
            player2.heal &&
            <div className={classes.cureWoundsPlayer2}>
              <CureWounds color="lime" size="300" delay={1} repeat={0} onComplete={() => updatePlayer({...player2, heal: false})}/>
            </div>
          }
          {
            player3.heal &&
            <div className={classes.cureWoundsPlayer3}>
              <CureWounds color="lime" size="300" delay={1} repeat={0} onComplete={() => updatePlayer({...player3, heal: false})}/>
            </div>
          }
          {
            player4.heal &&
            <div className={classes.cureWoundsPlayer4}>
              <CureWounds color="lime" size="300" delay={1} repeat={0} onComplete={() => updatePlayer({...player4, heal: false})}/>
            </div>
          }
          <Grid item xs={12}>
            <Button variant='contained' className={classes.fireballButton}  startIcon={<img src={fireballIcon} alt="fireball" />} onClick={() => castFireball()}>Fireball</Button>
            <Button variant='contained' className={classes.logoutButton} disabled={isPendingLogout} onClick={this.handleLogout}>Logout</Button>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={1}>
              <Grid item xs={12}><Typography variant="h4" gutterBottom className={classes.name}>{player1.player}</Typography></Grid>
              { this.renderButton( (player1.d4? d4_on: d4_off), "D4", () => updatePlayer({...player1, d4: !player1.d4}))}
              { this.renderButton( (player1.d6? d6_on: d6_off), "D6", () => updatePlayer({...player1, d6: !player1.d6}))}           
              { this.renderButton( (player1.inspiration? inspiration_on: inspiration_off), "Inspiration", () => updatePlayer({...player1, inspiration: !player1.inspiration}))}           
              { this.renderButton( healingIcon, "Cure Wounds", () => updatePlayer({...player1, heal: true}))}
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={1}>
              <Grid item xs={12}><Typography variant="h4" gutterBottom className={classes.name}>{player2.player}</Typography></Grid>
              { this.renderButton( (player2.d4? d4_on: d4_off), "D4", () => updatePlayer({...player2, d4: !player2.d4}))}
              { this.renderButton( (player2.d6? d6_on: d6_off), "D6", () => updatePlayer({...player2, d6: !player2.d6}))}           
              { this.renderButton( (player2.inspiration? inspiration_on: inspiration_off), "Inspiration", () => updatePlayer({...player2, inspiration: !player2.inspiration}))} 
              { this.renderButton( healingIcon, "Cure Wounds", () => updatePlayer({...player2, heal: true}))}
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={1}>
              <Grid item xs={12}><Typography variant="h4" gutterBottom className={classes.name}>{player3.player}</Typography></Grid>
              { this.renderButton( (player3.d4? d4_on: d4_off), "D4", () => updatePlayer({...player3, d4: !player3.d4}))}
              { this.renderButton( (player3.d6? d6_on: d6_off), "D6", () => updatePlayer({...player3, d6: !player3.d6}))}           
              { this.renderButton( (player3.inspiration? inspiration_on: inspiration_off), "Inspiration", () => updatePlayer({...player3, inspiration: !player3.inspiration}))}
              { this.renderButton( healingIcon, "Cure Wounds", () => updatePlayer({...player3, heal: true}))}
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={1}>
              <Grid item xs={12}><Typography variant="h4" gutterBottom className={classes.name}>{player4.player}</Typography></Grid>
              { this.renderButton( (player4.d4? d4_on: d4_off), "D4", () => updatePlayer({...player4, d4: !player4.d4}))}
              { this.renderButton( (player4.d6? d6_on: d6_off), "D6", () => updatePlayer({...player4, d6: !player4.d6}))}           
              { this.renderButton( (player4.inspiration? inspiration_on: inspiration_off), "Inspiration", () => updatePlayer({...player4, inspiration: !player4.inspiration}))}
              { this.renderButton( healingIcon, "Cure Wounds", () => updatePlayer({...player4, heal: true}))}
            </Grid>
          </Grid>
        </Grid>
      }
      <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={snackbarOpen}
          autoHideDuration={6000}
          onClose={this.handleSnackbarClose}
          message={snackbarMessage}
          action={
            <React.Fragment>
              <IconButton size="small" aria-label="close" color="inherit" onClick={this.handleSnackbarClose}>
                <Close fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />
      </div>
    )
  }
}

const styles = (theme) => ({
  App: {
    backgroundColor: "transparent",
    minHeight: "100vh",
    minWidth: "100vw",
  },
  App_dark: {
    backgroundColor: "#292929",
    minHeight: "100vh",
    minWidth: "100vw",
  },
  loggedInRoot: {
    width: '100vw',
    maxWidth: '822px',
    flexGrow: 1,
    padding: '10px',
  },
  playerContainer: {
    flexGrow: 1,
  },
  playerButton: {
    width: '100%',
  },
  name: {
    color: '#e0e0e0',
    textAlign: 'center'
  },
  buttonContainer: {
    maxWidth: '500px'
  },
  formContainer: {
    backgroundColor: 'white',
    maxWidth: '500px'
  },
  logoutButton: {
    float: "right"
  },
  fireballButton: {
    float: "left"
  },
  fireball: {
    position: "fixed",
    top: "450px",
    left: "450px",
    zIndex: "2",
  },
  fireballLoggedIn: {
    position: "fixed",
    top: "0px",
    left: "0px",
    zIndex: "2",
  },
  cureWoundsPlayer1: {
    position: "absolute",
    top: "-20px",
    left: "15%",
    zIndex: '2',
  },
  cureWoundsPlayer2: {
    position: "absolute",
    top: "158px",
    left: "15%",
    zIndex: '2',
  },
  cureWoundsPlayer3: {
    position: "absolute",
    top: "296px",
    left: "15%",
    zIndex: '2',
  },
  cureWoundsPlayer4: {
    position: "absolute",
    top: "454px",
    left: "15%",
    zIndex: '2',
  },
});

Landing.propTypes = {
  player1: PropTypes.object.isRequired,
  player2: PropTypes.object.isRequired,
  player3: PropTypes.object.isRequired,
  player4: PropTypes.object.isRequired,
  fireball: PropTypes.bool.isRequired,
  isFailedLogin: PropTypes.bool.isRequired,
  isPendingLogin: PropTypes.bool.isRequired,
  isPendingLogout: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  updatePlayer: PropTypes.func.isRequired,
  quenchFireball: PropTypes.func.isRequired,
  castFireball: PropTypes.func.isRequired,
  resetPassword: PropTypes.func.isRequired,
}

export default withStyles(styles)(Landing);