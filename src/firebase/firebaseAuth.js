import { auth } from './firebaseInit';
import store from '../store/store';
import { loginChanged } from '../store/actions';

export const doSignInWithEmailAndPassword = (email, password) => {
  return auth.signInWithEmailAndPassword(email, password);
}

export const doSignOut = () => {
  return auth.signOut();
}

export const doPasswordReset = (email) => {
  return auth.sendPasswordResetEmail(email);
}

export const authObserver = auth.onAuthStateChanged(user => {
  if(user)
    store.dispatch(loginChanged(true));
  else
    store.dispatch(loginChanged(false));
});