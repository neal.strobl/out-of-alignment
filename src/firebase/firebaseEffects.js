import { db } from './firebaseInit';
import store from '../store/store';
import { fireballUpdated, error } from '../store/actions';

let fireball = db.collection('effects').doc('fireball');

/** Observer functions for effects (receive database updates) 
 */
export const fireball_observer = fireball.onSnapshot(docSnapshot => {
  let data = docSnapshot.data();
  //dispatch action to update store
  store.dispatch(fireballUpdated(data.cast));
}, err => {
  store.dispatch(error(err));
});

export const doUpdateFireball = (triggered) => {
  return fireball.update({cast: triggered});
};