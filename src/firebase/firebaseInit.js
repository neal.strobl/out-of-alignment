import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/app'
import 'firebase/database';
import 'firebase/firestore';

const config = {
  apiKey: "AIzaSyBRwV78jWdvPNIOd9-M1EKpCa1d98ifpGA",
  authDomain: "out-of-alignment.firebaseapp.com",
  databaseURL: "https://out-of-alignment.firebaseio.com",
  projectId: "out-of-alignment",
  storageBucket: "out-of-alignment.appspot.com",
  messagingSenderId: "623666521491",
  appId: "1:623666521491:web:b24272d6375f8cda9b7466"
};

const hostingURL = "https://out-of-alignment.web.app";

if(!firebase.apps.length) {
  firebase.initializeApp(config);
}

const auth = firebase.auth();
const db = firebase.firestore();

export {
  auth,
  db,
  hostingURL
}