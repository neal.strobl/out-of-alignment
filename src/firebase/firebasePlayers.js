import { db } from './firebaseInit';
import store from '../store/store';
import { playerUpdated, error } from '../store/actions';

let neal = db.collection('players').doc('4DoKgTYReBkKikqS2Ni4');
let mark = db.collection('players').doc('6ljUQYePxKRNVkDtHKCc');
let caroline = db.collection('players').doc('DX62sU7Wf4Skyns8q85S');
let greg = db.collection('players').doc('MbtZ0v8YXKGdIy9aNKYV');

/** Observer functions for players (receive database updates) 
 *  Call when component is mounting
 */
export const caroline_observer = caroline.onSnapshot(docSnapshot => {
  let data = docSnapshot.data();
  //dispatch action to update store
  store.dispatch(playerUpdated(data.character, data.d4, data.d6, data.inspiration, data.player, data.heal));
}, err => {
  store.dispatch(error(err));
});

export const greg_observer = greg.onSnapshot(docSnapshot => {
  let data = docSnapshot.data();
  store.dispatch(playerUpdated(data.character, data.d4, data.d6, data.inspiration, data.player, data.heal));
}, err => {
  store.dispatch(error(err));
});

export const mark_observer = mark.onSnapshot(docSnapshot => {
  let data = docSnapshot.data();
  store.dispatch(playerUpdated(data.character, data.d4, data.d6, data.inspiration, data.player, data.heal));
}, err => {
  store.dispatch(error(err));
});

export const neal_observer = neal.onSnapshot(docSnapshot => {
  let data = docSnapshot.data();
  store.dispatch(playerUpdated(data.character, data.d4, data.d6, data.inspiration, data.player, data.heal));
}, err => {
  store.dispatch(error(err));
});

/**
 * Sends new player data to server 
 * @param {character,d4,d6,inspiration,player} data 
 */
export const doUpdatePlayerDBEntry = (data) => {
  switch(data.player){
    case "Caroline":
      return caroline.update(data);
    case "Greg":
      return greg.update(data);
    case "Mark":
      return mark.update(data);
    case "Neal": 
      return neal.update(data);
    default:
      return {};
  }
};