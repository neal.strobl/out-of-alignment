import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";

import {
  CssBaseline,
  withStyles }from '@material-ui/core';

import { GameMain, PartyMain } from './pages/IconsLayout';
import Landing from './pages/Landing';

import { 
  login,
  logout,
  resetPassword,
  updatePlayer, 
  castFireball,
  quenchFireball} from './store/actions';

import * as selectors from './store/selectors';

import * as routes from './store/constants/routes';

import './App.css';

class App extends Component {

  render() {
    const { 
      caroline,
      neal,
      greg,
      mark,
      isFailedLogin,
      isPendingLogin,
      isPendingLogout,
      updatePlayer,
      isLoggedIn,
      quenchFireball,
      castFireball,
      fireball,
      login,
      logout,
      resetPassword,
    } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
          <Router>
            <Switch>
              <Route exact path={routes.GAME}>
                <GameMain 
                  player1={caroline} 
                  player2={neal} 
                  player3={greg} 
                  player4={mark} 
                  fireball={fireball}/>
              </Route>
              <Route exact path={routes.PARTY}>
                <PartyMain 
                  player1={caroline} 
                  player2={neal} 
                  player3={greg} 
                  player4={mark} 
                  fireball={fireball}/>
              </Route>
              <Route>
                <Landing 
                  player1={caroline} 
                  player2={neal} 
                  player3={greg} 
                  player4={mark} 
                  fireball={fireball} 
                  isFailedLogin={isFailedLogin}
                  isPendingLogin={isPendingLogin}
                  isPendingLogout={isPendingLogout}
                  isLoggedIn={isLoggedIn}
                  updatePlayer={updatePlayer}
                  quenchFireball={quenchFireball}
                  castFireball={castFireball}
                  login={login}
                  logout={logout}
                  resetPassword={resetPassword}
                  />
              </Route>
            </Switch>
          </Router>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  caroline: selectors.getCaroline(state),
  neal: selectors.getNeal(state),
  greg: selectors.getGreg(state),
  mark: selectors.getMark(state),
  errors: selectors.getErrors(state),
  isFailedLogin: selectors.isFailedLogin(state),
  isPendingLogin: selectors.isPendingLogin(state),
  isPendingLogout: selectors.isPendingLogout(state),
  isLoggedIn: selectors.isLoggedIn(state),
  fireball: selectors.getFireball(state)
});

const mapDispatchToProps = (dispatch) => ({
  login: bindActionCreators(login,dispatch),
  logout: bindActionCreators(logout,dispatch),
  updatePlayer: bindActionCreators(updatePlayer,dispatch),
  resetPassword: bindActionCreators(resetPassword,dispatch),
  castFireball: bindActionCreators(castFireball,dispatch),
  quenchFireball: bindActionCreators(quenchFireball,dispatch),
});

const styles = (theme) => ({
});

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(App));
