import React, { Component } from 'react';
import PropTypes from "prop-types";

import { withStyles }from '@material-ui/core';

import Confetti from 'react-dom-confetti';

import d4_off from '../images/icons/d4-off.png';
import d4_on from '../images/icons/d4-on.png';
import d6_off from '../images/icons/d6-off.png';
import d6_on from '../images/icons/d6-on.png';
import inspiration_off from '../images/icons/inspiration-off.png';
import inspiration_on from '../images/icons/inspiration-on.png';

import {CONFETTI_CONFIG, CureWounds, HealSound, PartySound } from '../store/constants/util';

class Icons extends Component {

  componentDidUpdate(prevProp) {
    if( (!prevProp.d4 && this.props.d4) || 
        (!prevProp.d6 && this.props.d6) || 
        (!prevProp.inspiration && this.props.inspiration) ){
      PartySound.play(0.3);
    }
  }

  /**
   * Render Icon as D4, D6, or Inspiration based on params
   * @param {number} type 0 for D4, 1 for D6, 2 for Inspiration
   * @param {boolean} enabled true for pink, false for black
   */
  renderIcon(type, enabled){
    const{
      classes
    } = this.props;
    switch(type){
      case 0:
          return (enabled)?  <img src={d4_on} className={classes.d4} alt="d4"/> : <img src={d4_off} className={classes.d4} alt="d4"/>;
      case 1:
          return (enabled)?  <img src={d6_on} className={classes.d6} alt="d6"/> : <img src={d6_off} className={classes.d6} alt="d6"/>;
      case 2:
          return (enabled)?  <img src={inspiration_on} className={classes.inspiration} alt="inspiration"/> : <img src={inspiration_off} className={classes.inspiration} alt="inspiration"/>;
      default:
        return '';
    }
  }

  renderConfetti = (trigger, className) => {
    return <Confetti active={ trigger } config={ CONFETTI_CONFIG } className={className}/>
  }

  render() {
    const { 
      classes,
      className,
      d4,
      d6,
      inspiration,
      heal
    } = this.props;

    return (
      <div className={className}>
        { this.renderIcon(0, d4) } { this.renderConfetti(d4, classes.d4) }
        { this.renderIcon(1, d6) } { this.renderConfetti(d6, classes.d6) }
        { this.renderIcon(2, inspiration) } { this.renderConfetti(inspiration, classes.inspiration) }
        {
          heal &&
          <div className={classes.cureWounds}>
            <CureWounds color="lime" size="250" delay={0} repeat={0} onStart={() => HealSound.play(0.3)}/>
          </div>
        }
      </div>
    )
  }
}

const styles = (theme) => ({
  cureWounds: {
    position: "absolute",
    top: "-70px",
    left: "120px",
  },
  d4: {
    position: "absolute",
    top: "0px",
    left: "0px",
  },
  d6: {
    position: "absolute",
    top: "43px",
    left: "0px",
  },
  inspiration: {
    position: "absolute",
    top: "78px",
    left: "0px",
  }
});

Icons.propTypes = {
  d4: PropTypes.bool.isRequired,
  d6: PropTypes.bool.isRequired,
  inspiration: PropTypes.bool.isRequired,
  heal: PropTypes.bool.isRequired,
};

Icons.defaultProps = {
  d4: true,
  d6: true,
  inspiration: true,
  heal: false,
}

export default withStyles(styles)(Icons);
